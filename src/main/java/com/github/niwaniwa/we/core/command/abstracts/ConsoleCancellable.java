package com.github.niwaniwa.we.core.command.abstracts;

/**
 * Consoleからの実行をブロック
 * @author niwaniwa
 *
 */
public interface ConsoleCancellable {
}
