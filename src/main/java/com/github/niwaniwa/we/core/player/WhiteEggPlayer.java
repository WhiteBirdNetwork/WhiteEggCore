package com.github.niwaniwa.we.core.player;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.github.niwaniwa.we.core.WhiteEggCore;
import com.github.niwaniwa.we.core.api.WhiteEggAPI;
import com.github.niwaniwa.we.core.api.callback.Callback;
import com.github.niwaniwa.we.core.command.toggle.ToggleSettings;
import com.github.niwaniwa.we.core.json.JsonManager;
import com.github.niwaniwa.we.core.player.rank.Rank;
import com.github.niwaniwa.we.core.twitter.PlayerTwitterManager;
import com.github.niwaniwa.we.core.twitter.TwitterManager;
import com.github.niwaniwa.we.core.util.Util;
import com.github.niwaniwa.we.core.util.lib.Vanish;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.TwitterException;
import twitter4j.auth.AccessToken;

/**
 * WhitePlayerの実装クラス
 * @author niwaniwa
 *
 */
public class WhiteEggPlayer extends EggPlayer {

	private static WhiteEggAPI api = WhiteEggCore.getAPI();

	private Player player;
	private final List<ToggleSettings> toggle = new ArrayList<>();
	private final List<Rank> ranks = new ArrayList<>();
	private AltAccount accounts;
	private boolean isVanish;
	private TwitterManager twitter;
	// not use database
	private final File path = new File(WhiteEggCore.getInstance().getDataFolder() + File.separator + "players" + File.separator);

	public WhiteEggPlayer(Player player){
		super(player);
		this.player = player;
		this.isVanish = false;
		this.twitter = new PlayerTwitterManager(this);
		ToggleSettings.getList().forEach(list -> toggle.add(list.clone()));
		this.accounts = new AltAccount();
	}

	@Override
	public String getPrefix() {
		StringBuilder sb = new StringBuilder();
		this.getRanks().forEach(rank -> sb.append(rank.getPrefix()));
		return sb.toString();
	}

	@Override
	public List<Rank> getRanks() {
		return ranks;
	}

	@Override
	public boolean addRank(Rank rank) {
		if(ranks.contains(rank)){ return false; }
		return ranks.add(rank);
	}

	@Override
	public boolean removeRank(Rank rank) {
		return ranks.remove(rank);
	}

	@Override
	public boolean isVanish() {
		return isVanish;
	}

	@Override
	public boolean vanish() {
		return Vanish.hide(this);
	}

	@Override
	public boolean show() {
		return Vanish.show(this);
	}

	@Override
	public void setVanish(boolean b) {
		this.isVanish = b;
	}

	/**
	 * アカウントの追加
	 * @param player
	 */
	protected void addAccount(WhitePlayer player){
		if(player.equals(this)){ return; }
		if(accounts.contains(player)){ return; }
		accounts.add(player.getUniqueId());
	}

	/**
	 * アカウントの削除
	 * @param player
	 */
	protected void removeAccount(WhitePlayer player){
		if(player.equals(this)){ return; }
		if(!accounts.contains(player)){ return; }
		accounts.remove(player);
	}

	@Override
	public boolean saveVariable(String jsonString) {
		JsonObject j = jm.createJsonObject(jsonString);
		JsonObject json = j.getAsJsonObject("WhiteEggPlayer");
		JsonObject player = json.getAsJsonObject("player");
		this.isVanish = player.get("isvanish").getAsBoolean();
		if(json.get("twitter").isJsonObject()){
			JsonObject tw = json.getAsJsonObject("twitter");
			this.getTwitterManager().setAccessToken(
					new AccessToken(tw.get("accesstoken").getAsString(), tw.get("accesstokensecret").getAsString()));
		}
		this.setRank(player);
		this.setToggle(player);
		AltAccount.parser(player);
		return true;
	}

	private void setRank(JsonObject j){
		if(j.get("rank") != null){
			JsonArray rank = j.getAsJsonArray("rank");
			for (Object rank1 : rank) {
				if(!(rank1 instanceof JsonObject)){ continue; }
				Rank r = Rank.parserRank(Util.toMap(((JsonObject) rank1).toString()));
				if(!Rank.check(r)){ continue; }
				this.addRank(r);
			}
		}
	}

	private void setToggle(JsonObject json){
		this.getToggleSettings().clear();
		JsonObject t = json.getAsJsonObject("toggles");
		for(Entry<String, JsonElement> entry : t.entrySet()){
			JsonObject toggleJ = entry.getValue().getAsJsonObject();
			ToggleSettings instance = ToggleSettings.deserializeJ(toggleJ);
			if(instance == null){ continue; }
			if(!ToggleSettings.containsInstance(instance)){ continue; }
			this.getToggleSettings().add(instance);
		}
	}

	public void setName(String name){
		player.setCustomName("");
	}

	public void tweet(String[] tweet){
		twitter.tweet(tweet);
	}

	private JsonManager jm = new JsonManager();

	@Override
	public boolean reload() {
		this.save();
		return this.load();
	}

	@Override
	public boolean load() {
		if(api.useDataBase()){
			// sql
			return true;
		}
		// local
		JsonObject json = jm.getJson(new File(path + File.separator + this.getUniqueId().toString() + ".json"));
		if(json == null){ return false; }
		this.saveVariable(json.toString());
		return true;
	}

	@Override
	public boolean save() {
		if(api.useDataBase()){
			// sql
			return true;
		}
		JsonObject json = unionJson();
		return jm.writeJson(path, getUniqueId().toString() + ".json", json);
	}

	public void saveTask(){
		new BukkitRunnable() {
			@Override
			public void run() {
				save();
			}
		}.runTaskLaterAsynchronously(WhiteEggCore.getInstance(), 1);
	}

	private JsonObject unionJson(){
		Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		String jsonString = gson.toJson(this.serialize());
		JsonObject serialize = jm.createJsonObject(jsonString);
		JsonObject fileJson = jm.getJson(new File(path, this.getUniqueId().toString() + ".json"));
		if(fileJson == null){ return serialize; }
		for(Entry<String, JsonElement> entry : fileJson.entrySet()){
			if(!entry.getKey().equalsIgnoreCase(this.getClass().getSimpleName())){
				serialize.add(entry.getKey(), entry.getValue());
			}
		}
		return serialize;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> result = new HashMap<>();
		Map<String, Object> player = new HashMap<>();
		Map<String, Object> white = new HashMap<>();
		Map<String, Object> toggle = new HashMap<>();
		this.getToggleSettings().forEach(list -> toggle.put(list.getPlugin().getName(), list.serialize()));
		player.put("name", this.getName());
		player.put("uuid", this.getUniqueId().toString());
		player.put("rank", this.getRanks()); // TODO: 必要のない情報が入ってしまう
		player.put("isvanish", this.isVanish);
		player.put("toggles", toggle);
		player.put("lastonline", new Date()+":"+Bukkit.getServerName());
		player.put("address", this.getAddress());
		player.put("account", this.getAccounts().get());
		result.put("player", player);
		result.put("ver", WhiteEggCore.getInstance().getDescription().getVersion());
		result.put("twitter", this.getTwitterManager().getAccessToken() == null ? "null" : this.serializeTwitter());
		white.put(this.getClass().getSimpleName(), result);
		return white;
	}

	private Map<String, Object> serializeTwitter() {
		Map<String, Object> result = new HashMap<>();
		result.put("accesstoken", getTwitterManager().getAccessToken().getToken());
		result.put("accesstokensecret", getTwitterManager().getAccessToken().getTokenSecret());
		return result;
	}

	@Override
	public TwitterManager getTwitterManager() {
		return twitter;
	}

	@Override
	public List<ToggleSettings> getToggleSettings() {
		return toggle;
	}

	@Override
	public String toString() {
		return this.serialize().toString();
	}

	public AltAccount getAccounts(){
		return accounts;
	}

	@Override
	public boolean clear() {
		this.toggle.clear();
		this.twitter = new PlayerTwitterManager(this);
		this.ranks.clear();
		return true;
	}

	@Override
	public void updateStatus(StatusUpdate update) {
		try {
			twitter.getTwitter().updateStatus(update);
		} catch (TwitterException e) { e.printStackTrace(); }
	}

	@Override
	public void updateStatus(StatusUpdate update, Callback callback) {
		new BukkitRunnable() {
			@Override
			public void run() {
				try {
					callback.onTwitter((twitter.getTwitter().updateStatus(update) == null ? false : true));
				} catch (TwitterException e) { e.printStackTrace(); }
			}
		}.runTaskAsynchronously(WhiteEggCore.getInstance());
	}

	@Override
	public void updateStatus(String tweet) {
		twitter.tweet(tweet);
	}

	@Override
	public List<Status> getTimeLine() {
		try {
			return twitter.getTwitter().getHomeTimeline();
		} catch (TwitterException e) { e.printStackTrace(); }
		return new ArrayList<>(0);
	}

}
